import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the AddStaffPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-staff',
  templateUrl: 'add-staff.html',
})
export class AddStaffPage {
  addstaffForm: FormGroup;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddStaffPage');
  }

  ngOnInit(){
    this.initializeForm();
  }

  private initializeForm(){
    this.addstaffForm = new FormGroup({
      staffName: new FormControl(null, Validators.required),
      identityNumber: new FormControl(null, Validators.required),
      usernameNewStaff: new FormControl(null, Validators.required),
      passwordNewStaff: new FormControl(null, Validators.required),
      email:new FormControl(null, Validators.required),
      gender:new FormControl(null, Validators.required),
      category:new FormControl(null, Validators.required)
    });
  }

  onSubmitstaffForm(){
    let staff = this.addstaffForm.value;
    console.log(staff);
    this.api.addNewStaff(staff)
    .then(data => {
      if(data['status'] == 'success'){
        console.log(data);
      }
      else{
        console.log(data['status']);
      }
    })
    .catch(() => {
      console.log("Error submit products to API");
    })
  }

}
