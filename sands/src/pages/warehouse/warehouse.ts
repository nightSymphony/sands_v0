import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { FormGroup, FormControl, Validators } from '@angular/forms';

/**
 * Generated class for the WarehousePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-warehouse',
  templateUrl: 'warehouse.html',
})
export class WarehousePage {
  all_orders: any;
  
  @ViewChild('my_id') myIdRef: ElementRef;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider,
    public toastCtrl: ToastController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WarehousePage');
  }

  ionViewWillEnter(){
    this.all_orders = [];
    this.api.getAllOrders()
    .then(data => {
      if(data['status'] == "failed fetch data"){
        this.presentToast("No Order Right Now");
      }
      else{
        if(data['all_orders'][0]){
          this.all_orders = data['all_orders'];
          console.log(this.all_orders);
        }
        else{
          console.log('Order is Empty');
        }
      }
      
    })
  }

  updateStatus(){
    let id = this.myIdRef.nativeElement.innerText;
    console.log(id);

    this.api.updateStatus(id)
    .then(data => {
      if(data['status'] == 'success'){
        console.log(data);
      }
      else{
        console.log(data['status']);
      }
    })
    .catch(() => {
      console.log("Error submit products to API");
    })
    
  }
  diklikno(){
    console.log("nononoo");
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
