import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the IncomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-income',
  templateUrl: 'income.html',
})
export class IncomePage {
    transactionList;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.initializeTransactionList();
  }

  initializeTransactionList(){
      this.transactionList = [
          {custName: 'Ferliana Dwitama',
            delivDate: '24 September 2018',
            price: '34000',
            notes: 'Tambahkan cinta',
            children: [{
                namaBarang: 'Jus Mangga',
                qty: '1',
                price: '10000'
            },
            {
                namaBarang: 'Jus Alpukat',
                qty: '1',
                price: '12000'
            },
            {
                namaBarang: 'Jus Jambu',
                qty: '1',
                price: '11000'

            }
        ]},
          {custName: 'Ivan Ferdino',
            delivDate: '25 September 2018',
            price: '12000',
            notes: 'Gula banyakan',
            children: [
            {
                namaBarang: 'Jus Alpukat',
                qty: '1',
                price: '12000'
            }
            ]},
          {custName: 'Nathanael Gilbert',
            delivDate: '25 September 2018',
            price: '44000',
            children: [{
                namaBarang: 'Jus Mangga',
                qty: '2',
                price: '10000'
            },
            {
                namaBarang: 'Jus Alpukat',
                qty: '4',
                price: '12000'
            },
        ]},
          {custName: 'Hermawan Sudono',
            delivDate: '26 September 2018',
            price: '60000'},
          {custName: 'Evan Hadinata Lesmana',
            delivDate: '27 September 2018',
            price: '110000'},
          {custName: 'Martha Saphira',
            delivDate: '27 September 2018',
            price: '34000'}
      ];
  }

  getTransactionList(ev){
      this.initializeTransactionList();

      var val = ev.target.value;

      if(val && val.trim() != ''){
          this.transactionList = this.transactionList.filter((item) => {
              return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
      }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IncomePage');
  }

}
