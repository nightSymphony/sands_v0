import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';
import { OwnerPage } from '../owner/owner';
import { WarehousePage } from '../warehouse/warehouse';
import { ShipperPage } from '../shipper/shipper';
import { IncomePage } from '../income/income';
import { SpendingPage } from '../spending/spending';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  responseData: any;
  buttonColor: string = '#2D3142';
  registerForm: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider,
    public toastCtrl: ToastController
  ) {
  }

  ngOnInit(){
    this.initializeForm();
  }

  private initializeForm(){
    this.registerForm = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      
    });
  }

  changeColor () {
    this.buttonColor = '#EEC643';
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  onSubmitForm(){
    console.log(this.registerForm.value);
    let username = this.registerForm.get('username').value;
    let password = this.registerForm.get('password').value;
    // if(username == 'owner'){
    //   this.navCtrl.push(OwnerPage);
    // }
    // else if(username == 'warehouse'){
    //   console.log('Trying to access warehouse menu');
    // }
    // else if(username == 'courier'){
    //   console.log('Trying to access courier menu');
    // }
    let _user = {
      'username': username,
      'password': password
    }
    this.api.login(_user)
    .then((data) =>{
      if(data['status'] == 'success'){
        console.log('Login Sukses');
        console.log(data['users'][0].page_code);
        if(data['users'][0].page_code == 1){
          this.navCtrl.setRoot(OwnerPage);
        }
        else if(data['users'][0].page_code == 2){
          this.navCtrl.setRoot(WarehousePage);
        }
        else if(data['users'][0].page_code == 3){
          this.navCtrl.setRoot(ShipperPage);
        }
        // this.api.setCode().then(data => {
        //   //console.log(data['users']);
        //   //console.log(data[0].page_code);
        //   //console.log('Open Sidebar : ' + data['users']);
        //   // if(data[0].page_code == 1){
        //   //   this.navCtrl.setRoot(OwnerPage);
        //   // }
        // });
        //
        //this.navCtrl.push(OwnerPage);
      }
      else{
        this.presentToast("Invalid Username or Password");
        console.log('I love u');
      }
    })
    .catch(() => {
      console.log('Failed Login');
    })
  }

  // ci_onSubmit(){
  //   console.log(this.registerForm.value);
  //   let username = this.registerForm.get('username').value;
  //   let password = this.registerForm.get('password').value;
  //   let _user = {
  //     'username': username,
  //     'password': password
  //   }
    
  //   this.api.ci_login(_user, "/Api_login/loginAgent").then(
  //     result => {
  //       this.responseData = result;
  //       if(this.responseData.status==1){
  //         console.log(result);
  //         //console.log('BERHASIILL!!!!! '+this.responseData.result.username);
  //       }else {
  //         //this.navCtrl.push(LoginPage);
  //         this.presentToast("Invalid Username or Password");
  //       }
  //     },
  //     err => {}
  //   );
  // }
  
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
