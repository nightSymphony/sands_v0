import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IncomePage } from '../income/income';
import { SpendingPage } from '../spending/spending';

/**
 * Generated class for the FinanceReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-finance-report',
  templateUrl: 'finance-report.html',
})
export class FinanceReportPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinanceReportPage');
  }
  
  tab1root= IncomePage;
  tab2root= SpendingPage;

}
