import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SpendingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-spending',
  templateUrl: 'spending.html',
})
export class SpendingPage {
  spendingList;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initializeSpendingList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpendingPage');
  }
  
  initializeSpendingList(){
    this.spendingList= [
      {
        namaBarang: 'Buah Jambu',
        qty: '3 (Kg)',
        price: '36000',
        buyDate: '23 September 2018'
      },
      {
        namaBarang: 'Buah Semangka',
        qty: '5 (Kg)',
        price: '50000',
        buyDate: '23 September 2018'
      },
      {
        namaBarang: 'Buah Sirsak',
        qty: '2 (Kg)',
        price: '36000',
        buyDate: '24 September 2018'
      },
      {
        namaBarang: 'Botol',
        qty: '300 (Pcs)',
        price: '500000',
        buyDate: '24 September 2018'
      },
      {
        namaBarang: 'Sticker',
        qty: '10 (Lbr)',
        price: '130000',
        buyDate: '24 September 2018'
      },
      {
        namaBarang: 'Buah Alpukat',
        qty: '4 (Kg)',
        price: '80000',
        buyDate: '25 September 2018'
      },
      {
        namaBarang: 'Buah Jeruk',
        qty: '3 (Kg)',
        price: '30000',
        buyDate: '25 September 2018'
      },
      {
        namaBarang: 'Buah Jambu',
        qty: '3 (Kg)',
        price: '36000',
        buyDate: '25 September 2018'
      },
      {
        namaBarang: 'Buah Mangga',
        qty: '5 (Kg)',
        price: '60000',
        buyDate: '26 September 2018'
      },
      {
        namaBarang: 'Buah Jambu',
        qty: '3 (Kg)',
        price: '36000',
        buyDate: '26 September 2018'
      },
    ]
  }

}
