import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InputOrderPage } from './input-order';

@NgModule({
  declarations: [
    InputOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(InputOrderPage),
  ],
})
export class InputOrderPageModule {}
