import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ToastController } from 'ionic-angular';

import * as $ from 'jquery';
import { DatePicker } from '@ionic-native/date-picker';
import { FormGroup, Validators, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the InputOrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-input-order',
  templateUrl: 'input-order.html',
})
export class InputOrderPage {

  deliveryDate:any;
  orderForm: FormGroup;
  items: FormArray;
  all_item: {id: number, item_name: string}[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public datePicker: DatePicker,
    public api: ApiProvider,
    private formBuilder: FormBuilder,
    public toastCtrl: ToastController
  ) {
    this.all_item = [
      {id: 1, item_name: "Jus Alpukat"},
      {id: 3, item_name: "Jus Mangga"},
      {id: 4, item_name: "Jus Apel"},
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InputOrderPage');
    $('#calendar').hide();
  }
  
  showCalendar(){
    $('#calendar').show();
  }

  onDaySelect($event){
    console.log($event);
    console.log($event.year+'-'+$event.month+'-'+$event.date);
    $('#calendar').hide(); 
  }

  ngOnInit(){
    this.initializeForm();
  }

  private initializeForm(){
    this.orderForm = new FormGroup({
      //chooseItem: new FormControl(null, Validators.required),
      //qty: new FormControl(null, Validators.required),
      custName: new FormControl(null, Validators.required),
      deliveryDate: new FormControl(null, Validators.required),
      notes: new FormControl(null, Validators.required),
      items: this.formBuilder.array([this.createItem()])
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      chooseItem: '',
      qty: ''
    });
  }
  addItem(): void {
    this.items = this.orderForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  year: number;
  month: any;
  dt: any;
  getDate(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        console.log('Got Date: '+ date.toISOString);
        this.year = date.getFullYear();
        this.month = date.getMonth()+1;
        this.dt = date.getDate();

        if (this.dt < 10) {
          this.dt = '0' + this.dt;
        }
        if (this.month < 10) {
          this.month = '0' + this.month;
        }

        console.log('Selected Date is: '+this.year+'-' + this.month + '-'+this.dt);
        this.deliveryDate = this.year+'-' + this.month + '-'+this.dt;
        this.orderForm.value.deliveryDate = this.year+'-' + this.month + '-'+this.dt;
        //this.deliveryDate = date.getDay.toString;
        //console.log(deliveryDate);
      }
    );
  }

  onSubmitOrderForm(){
    let new_order = this.orderForm.value;
    console.log(new_order.items.length);
    // Insert ke Orders
    this.api.addNewOrder(new_order).then(data => {
      if(data['status'] == 'success'){
        console.log('Order: '+data);
        this.presentToast("Order Success");
      }
      else{
        console.log(data['status']);
        
      }
    }).catch(() => {
      console.log("Error submit products to API");
    });

    for(let i = 0; i < new_order.items.length; i++){
      // Insert ke Order Detail
      let the_order = new_order.items[i];
      this.api.addNewOrderDetail(the_order).then(data => {
        if(data['status'] == 'success'){
          console.log(data);
          console.log('KE DETAIL');
        }
        else{
          console.log(data['status']);
        }
      }).catch(() => {
        console.log("Error submit products to API");
      });
    }    
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  // onSubmitstaffForm(){
  //   let staff = this.addstaffForm.value;
  //   console.log(staff);
  //   this.api.addNewStaff(staff)
  //   .then(data => {
  //     if(data['status'] == 'success'){
  //       console.log(data);
  //     }
  //     else{
  //       console.log(data['status']);
  //     }
  //   })
  //   .catch(() => {
  //     console.log("Error submit products to API");
  //   })
  // }

}
