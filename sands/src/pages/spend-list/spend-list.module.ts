import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpendListPage } from './spend-list';

@NgModule({
  declarations: [
    SpendListPage,
  ],
  imports: [
    IonicPageModule.forChild(SpendListPage),
  ],
})
export class SpendListPageModule {}
