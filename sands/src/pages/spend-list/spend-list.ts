import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CameraOptions, Camera } from '@ionic-native/camera';

/**
 * Generated class for the SpendListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-spend-list',
  templateUrl: 'spend-list.html',
})
export class SpendListPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public camera:Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpendListPage');
  }

  imageUrl: string;
  onTakePhoto(){
    
    const options: CameraOptions = {
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
      }
      this.camera.getPicture(options)
      .then(
        (imageData) => {
          this.imageUrl = imageData;
          console.log(this.imageUrl);
      },
      (err) => {
        console.log(err);
      });
  }
}
