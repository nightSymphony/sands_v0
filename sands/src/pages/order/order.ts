import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

import * as $ from 'jquery';
import { DatePicker } from '@ionic-native/date-picker';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { CartPage } from '../cart/cart';
import { InputOrderPage } from '../input-order/input-order';

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {

  @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides ;

  deliveryDate:any;
  // SwipedTabsIndicator :any= null;
  // tabElementWidth_px :number= 50;
  // tabs:any=[];

  // orderForm: FormGroup;

  // tab1root= InputOrderPage;
  // tab2root= CartPage;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public datePicker: DatePicker
  ) {
    
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad OrderPage');
  //   $('#calendar').hide();
  // }

  // ngOnInit(){
  //   this.initializeForm();
  // }

  // private initializeForm(){
  //   this.orderForm = new FormGroup({
  //     custName: new FormControl(null, Validators.required),
  //     chooseItem: new FormControl(null, Validators.required),
  //     qty: new FormControl(null, Validators.required),
  //     deliveryDate: new FormControl(null, Validators.required),
  //     // date: new FormControl(null, Validators.required),
  //     totalPrice: new FormControl(null, Validators.required)
  //   });
  // }

  // ionViewDidEnter() {
  //   this.SwipedTabsIndicator = document.getElementById("indicator");
  // }

  // selectTab(index) {
  //   this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(100*index)+'%,0,0)';
  //   this.SwipedTabsSlider.slideTo(index, 500);
  // }

  // updateIndicatorPosition() {

  //     // this condition is to avoid passing to incorrect index
  //   if( this.SwipedTabsSlider.length()> this.SwipedTabsSlider.getActiveIndex())
  //   {
  //     this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.SwipedTabsSlider.getActiveIndex() * 100)+'%,0,0)';
  //   }

  //   }


  // animateIndicator($event) {
  //   if(this.SwipedTabsIndicator)
  //        this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress* (this.SwipedTabsSlider.length()-1))*100) + '%,0,0)';

  // }

  // getDate(){
  //   this.datePicker.show({
  //     date: new Date(),
  //     mode: 'date',
  //     androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
  //   }).then(
  //     date => {
  //       console.log('Got Date: ', date)
  //       //this.deliveryDate = date.getDay.toString;
  //       //console.log(deliveryDate);
  //     }
  //   );
  // }

  // showCalendar(){
  //   $('#calendar').show();
  // }

  // onDaySelect($event){
  //   console.log($event);
  //   console.log($event.year+'-'+$event.month+'-'+$event.date);
  //   $('#calendar').hide();
    
  // }
}
