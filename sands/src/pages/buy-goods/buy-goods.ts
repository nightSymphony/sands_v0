import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { CameraOptions, Camera } from '@ionic-native/camera';

/**
 * Generated class for the BuyGoodsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-buy-goods',
  templateUrl: 'buy-goods.html',
})
export class BuyGoodsPage {
  addbuyForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, public camera:Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuyGoodsPage');
  }

  ngOnInit(){
    this.initializeForm();
  }
  private initializeForm(){
    this.addbuyForm = new FormGroup({
      goodsName: new FormControl(null, Validators.required),
      qty: new FormControl(null, Validators.required),
      price: new FormControl(null, Validators.required),
     
    });
  }

  imageUrl: string;
  onTakePhoto(){
    
    const options: CameraOptions = {
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
      }
      this.camera.getPicture(options)
      .then(
        (imageData) => {
          this.imageUrl = imageData;
          console.log(this.imageUrl);
      },
      (err) => {
        console.log(err);
      });
  }
}
