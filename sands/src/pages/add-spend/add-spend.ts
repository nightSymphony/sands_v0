import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, Validators, FormControl } from '@angular/forms';

/**
 * Generated class for the AddSpendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-spend',
  templateUrl: 'add-spend.html',
})
export class AddSpendPage {
  addbuyForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddSpendPage');
  }
  
  ngOnInit(){
    this.initializeForm();
  }
  private initializeForm(){
    this.addbuyForm = new FormGroup({
      goodsName: new FormControl(null, Validators.required),
      qty: new FormControl(null, Validators.required),
      price: new FormControl(null, Validators.required),
     
    });
  }
}
