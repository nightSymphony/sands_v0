import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListDetailTransactionPage } from './list-detail-transaction';

@NgModule({
  declarations: [
    ListDetailTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(ListDetailTransactionPage),
  ],
})
export class ListDetailTransactionPageModule {}
