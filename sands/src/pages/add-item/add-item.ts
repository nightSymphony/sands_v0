import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the AddItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-item',
  templateUrl: 'add-item.html',
})
export class AddItemPage {
  additemForm: FormGroup;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddItemPage');
  }

  ngOnInit(){
    this.initializeForm();
  }

  private initializeForm(){
    this.additemForm = new FormGroup({
      itemName: new FormControl(null, Validators.required),
      itemPrice: new FormControl(null, Validators.required),
    });
  }

  onSubmitItemForm(){
    let product_name = this.additemForm.get('itemName').value;
    let product_price = this.additemForm.get('itemPrice').value;
    
    let item = {
      'product_name': product_name,
      'product_price': product_price
    }
    console.log(item);
    this.api.addNewProduct(item)
    .then(data => {
      if(data['status'] == 'success'){
        console.log(data);
      }
      else{
        console.log(data['status']);
      }
    })
    .catch(() => {
      console.log("Error submit products to API");
    })
  }

}
