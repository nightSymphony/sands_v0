import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SpendListPage} from '../spend-list/spend-list';
import { AddSpendPage } from '../add-spend/add-spend';


/**
 * Generated class for the InputSpendingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-input-spending',
  templateUrl: 'input-spending.html',
})
export class InputSpendingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InputSpendingPage');
  }
  tab1root=AddSpendPage;
  tab2root= SpendListPage;
}
