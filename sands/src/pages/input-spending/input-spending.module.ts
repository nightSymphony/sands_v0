import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InputSpendingPage } from './input-spending';

@NgModule({
  declarations: [
    InputSpendingPage,
  ],
  imports: [
    IonicPageModule.forChild(InputSpendingPage),
  ],
})
export class InputSpendingPageModule {}
