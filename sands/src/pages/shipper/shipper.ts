import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the ShipperPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shipper',
  templateUrl: 'shipper.html',
})
export class ShipperPage {
  ready_orders: any; 
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider,
    public toastCtrl: ToastController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShipperPage');
  }

  ionViewWillEnter(){
    this.ready_orders = [];
    this.api.getReadyOrders()
    .then(data => {
      if(data['status'] == "failed fetch data"){
        this.presentToast("No Order Right Now");
      }
      else{
        if(data['all_orders'][0]){
          this.ready_orders = data['all_orders'];
          console.log(this.ready_orders);
        }
        else{
          console.log('Order is Empty');
        }
      }
    })
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
