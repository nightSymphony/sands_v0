import { HttpClient } from '@angular/common/http';
import {Http, Headers} from '@angular/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
let apiUrl = 'http://localhost/api-sands';


@Injectable()
export class ApiProvider {
  _code = 2;
  _username = '';
  constructor(public http: HttpClient, public myHttp: Http) {
    console.log('Hello ApiProvider Provider');
  }

  // login(user): Promise<any>{
  //   let url = "https://www.urganizer.id/api/api_login.php";
  //   let param = {username: user.username, password: user.password};
  
  //   return new Promise((resolve, reject) => {
  //     this.http.post(url, JSON.stringify(param))
  //     .subscribe(data => {
  //       resolve(data);
  //       console.log(data);
  //       if(data['status'] == "error"){
  //         console.log('Invalid Username/Password')
  //       }
  //       else{
  //         this._code = data['users'][0].page_code;
  //         this._username = data['users'][0].username;
  //       }
  //       console.log(this._code);
  //     }, (err) =>{
  //       console.log('Error broo');
  //     })
  //   })
  // }
  login(user): Promise<any>{
    let url = "https://www.urganizer.id/api/api_login.php";
    let param = {username: user.username, password: user.password};
  
    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(param))
      .subscribe(data => {
        resolve(data);
        console.log(data);
        if(data['status'] == "error"){
          console.log('Invalid Username/Password')
        }
        else{
          this._code = data['users'][0].page_code;
          this._username = data['users'][0].username;
        }
        console.log(this._code);
      }, (err) =>{
        console.log('Error broo');
      })
    })
  }

  logout(_username){
    let url = "https://www.urganizer.id/api/api_logout.php";
    let param = {username: _username};
  
    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(param))
      .subscribe(data => {
        resolve(data);
        
        //this._code = data['users'][0].page_code;
        //console.log(this._code);
      }, (err) =>{
        console.log('Error broo');
      })
    })
  }

  getCurrentUser(){
    return this._username;
  }

  // setCode(){
  //   let connUrl = "http://localhost/api/api_check_privilege.php";
  //   return new Promise((resolve, reject) => {
  //     this.http.get(connUrl).subscribe(data => {
  //       console.log(data[0].user_type);
  //       this._code = data[0].user_type;
  //       //this._username = data[0].username;
  //       resolve(data);
  //     }, err => {
  //       reject(err);
  //     })
  //   })
  // }

  getCode(){
    //let userCode = 1;
    return this._code;
  }

  // checkConnection(){
  //   let connUrl = "http://localhost/api/api_check_privilege.php";
  //   return new Promise((resolve, reject) => {
  //     this.http.get(connUrl).subscribe(data => {
  //       resolve(data);
  //       //console.log(data[0].user_type);
  //     }, err => {
  //       reject(err);
  //     })
  //   })
  // }

  addNewProduct(product){
    let url = "https://www.urganizer.id/api/api_insert_new_product.php";
    let param = {_productName: product.product_name, _productPrice: product.product_price};
    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(param))
      .subscribe(data => {
        resolve(data);
        console.log(data);
      }, (err) =>{
        console.log('Error broo');
      })
    })
  }

  addNewStaff(staff){
    let url = "https://www.urganizer.id/api/api_insert_new_staff.php";
    let param = {staff: staff};
    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(param))
      .subscribe(data => {
        resolve(data);
        console.log(data);
      }, (err) =>{
        console.log('Error broo');
      })
    })
  }

  addNewOrder(new_order){
    let url = "https://www.urganizer.id/api/api_insert_new_order.php";
    let param = {new_order: new_order};
    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(param))
      .subscribe(data => {
        resolve(data);
        console.log(data);
      }, (err) =>{
        console.log('Error broo');
      })
    })
  }

  addNewOrderDetail(new_order){
    let url = "https://www.urganizer.id/api/api_insert_new_order_detail.php";
    let param = {new_order: new_order};
    console.log(new_order);
    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(param))
      .subscribe(data => {
        resolve(data);
        console.log(data);
      }, (err) =>{
        console.log('Error broo');
      })
    })
  }

  updateStatus(id){
    let url = "https://www.urganizer.id/api/api_update_order_status.php";
    let param = {id: id};
    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(param))
      .subscribe(data => {
        resolve(data);
        console.log(data);
      }, (err) =>{
        console.log('Error broo');
      })
    })
  }

  getAllOrders(){
    return new Promise((resolve, reject) => {
      this.http.get('https://www.urganizer.id/api/api_get_all_orders.php').subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        reject(err);
      })
    })
  }

  getReadyOrders(){
    return new Promise((resolve, reject) => {
      this.http.get('https://www.urganizer.id/api/api_get_ready_orders.php').subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        reject(err);
      })
    })
  }

  ci_login(credentials, type){
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.myHttp.post(apiUrl + type, JSON.stringify(credentials), {headers: headers})
        .subscribe(res => {
          resolve(res.json());
          //console.log('CODE FROM CI IS : '+res.json().result.page_code);
          //console.log(res.json());
          if(res.json().status == 1){
            this._code = res.json().result.page_code;
            this._username = res.json().result.username;
            console.log('CODE IS: '+this._username+' AND USERNAME IS: '+this._username);
          }
          else{
            console.log('No Response');
          }
          
        }, (err) => {
          reject(err);
        });
    });
  }
}
