import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthService {

  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }

  checkConnection(){
    let connUrl = "http://localhost/api/api_check_privilege.php";
    return new Promise((resolve, reject) => {
      this.http.get(connUrl).subscribe(data => {
        resolve(data);
        //console.log(data[0].user_type);
      }, err => {
        reject(err);
      })
    })
  }
}
