import { MbscModule } from '@mobiscroll/angular';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CalendarModule } from 'ionic3-calendar-en';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { OwnerPage } from '../pages/owner/owner';
import { SlidesPage } from '../pages/slides/slides';
import { AddItemPage } from '../pages/add-item/add-item';
import { FinanceReportPage } from '../pages/finance-report/finance-report';
import { OrderPage } from '../pages/order/order';
import { AddStaffPage } from '../pages/add-staff/add-staff';
import { IncomePage } from '../pages/income/income';
import { SpendingPage } from '../pages/spending/spending'; 
import { CartPage } from '../pages/cart/cart'; 
import { InputOrderPage } from '../pages/input-order/input-order'; 

import { ApiProvider } from '../providers/api/api';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { AuthService } from '../services/AuthService';
import { WarehousePage } from '../pages/warehouse/warehouse';
import { ShipperPage } from '../pages/shipper/shipper';
import { DatePicker } from '@ionic-native/date-picker';
import { BuyGoodsPage} from '../pages/buy-goods/buy-goods';
import {Camera} from '@ionic-native/camera';
import {AddSpendPage} from '../pages/add-spend/add-spend';
import {SpendListPage} from '../pages/spend-list/spend-list';
import {InputSpendingPage } from '../pages/input-spending/input-spending';
import { TooltipsModule } from 'ionic-tooltips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    OwnerPage,
    AddItemPage,
    AddStaffPage,
    FinanceReportPage,
    OrderPage,
    SlidesPage,
    WarehousePage,
    ShipperPage,
    BuyGoodsPage,
    IncomePage,
    SpendingPage,
    CartPage,
    InputOrderPage,
    SpendListPage,
    InputSpendingPage,
    AddSpendPage,
    SpendListPage,
    InputSpendingPage 
  ],
  imports: [ 
    MbscModule, 
    FormsModule, 
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      tabsPlacement: 'top',
      scrollAssist: true,
      autoFocusAssist: false
    }),
    HttpModule,
    HttpClientModule,
    CalendarModule,
    IonicStorageModule.forRoot(),
    TooltipsModule,
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    OwnerPage,
    AddItemPage,
    AddStaffPage,
    FinanceReportPage,
    OrderPage,
    SlidesPage,
    BuyGoodsPage,
    WarehousePage,
    ShipperPage,
    IncomePage,
    SpendingPage,
    CartPage,
    InputOrderPage,
    AddSpendPage,
    SpendListPage,
    InputSpendingPage,
    ShipperPage,
    WarehousePage 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    AuthService,
    DatePicker,
    Camera
  ]
})
export class AppModule {}
