import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController,NavController } from 'ionic-angular';

import { LoginPage } from '../pages/login/login';
import { OwnerPage } from '../pages/owner/owner';
import { AddItemPage } from '../pages/add-item/add-item';
import { AddStaffPage } from '../pages/add-staff/add-staff';
import { FinanceReportPage } from '../pages/finance-report/finance-report';
import { OrderPage } from '../pages/order/order';
import { AuthService } from '../services/AuthService';
import { ApiProvider } from '../providers/api/api';
import { SlidesPage } from '../pages/slides/slides';
import { IncomePage } from '../pages/income/income';
import { SpendingPage } from '../pages/spending/spending';
import * as $ from 'jquery';
import { CartPage } from '../pages/cart/cart';
import { InputOrderPage } from '../pages/input-order/input-order';
import { InputSpendingPage  } from '../pages/input-spending/input-spending';
import { AddSpendPage } from '../pages/add-spend/add-spend';
import { SpendListPage } from '../pages/spend-list/spend-list';
import { WarehousePage } from '../pages/warehouse/warehouse';
import { ShipperPage } from '../pages/shipper/shipper';


@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  addItemPage = AddItemPage;
  addStaffPage = AddStaffPage;
  financeReportPage = FinanceReportPage;
  ownerPage = OwnerPage;
  loginPage = LoginPage;
  orderPage = OrderPage;
  slidesPage = SlidesPage;
  inputspendingPage  = InputSpendingPage;
  incomePage = IncomePage;
  spendingPage = SpendingPage;
  cartPage = CartPage;
  inputOrderPage = InputOrderPage;
  addSpendPage = AddSpendPage;
  spendListPage = SpendListPage;
  warehousePage = WarehousePage;
  shipperPage = ShipperPage;
  

  @ViewChild('sideMenuContent') navCtrl: NavController;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen , 
    public menuCtrl: MenuController,
    public api: ApiProvider
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  onLoad(page: any) {
    this.navCtrl.setRoot(page);
    this.menuCtrl.close();
  }

  check_users(){
    return this.api.getCode();
  }

  logout(){
    let currentUser = this.api.getCurrentUser();
    console.log(currentUser+' is logging out');
    this.api.logout(currentUser)
    .then(data => {
      this.navCtrl.setRoot(this.loginPage);
    });
  }
}

